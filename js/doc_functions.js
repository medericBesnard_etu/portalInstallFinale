const CHEZMEME_BASE_URL = "http://chezmeme.com/sso/api/v1.5/";
const BASE_DOC_BASE_URL = "http://etudiant2.chezmeme.com/base-documentaire/public/api/";

var inactivityTimeout;
var fileLink;

// Uncomment the following to remove console logs in production
// console.log = function() {};

/**
 * Display page specified as parameter.
 * @param {page} : id of the wanted page.
 */
function display(page) {
  console.log('FILES APP : function display(page)');
  $('.active.page').addClass('d-none');
  $('.active.page').removeClass('active');
  $('#' + page).addClass('active');
  $('#' + page).removeClass('d-none');
  if (page != "login") {
    $('#header').removeClass('d-none');
    $('#header').addClass('active');
  }
}

/**
 * Load files from API "Base Documentaire".
 * @param {token} : Authenticated user token.
 */
function loadFiles(token) {
  console.log('FILES APP : function loadFiles(token)');
  // Call API to list files
  $.ajax({
      method: "GET",
      url: BASE_DOC_BASE_URL + token + "/files",
      dataType: "json"
    })
    .done(function (filesData) {
      displayFiles(filesData);
    })
    .fail(function () {
      console.log("FILES APP : file list not loaded. An error occured.");
    });
}

/**
 * Load form content from API "Base Documentaire".
 * @param {token} : Authenticated user token.
 */
function loadTypes(token) {
  console.log('FILES APP : function loadFormContent(token)');
  // Call API
  $.ajax({
      method: "GET",
      url: BASE_DOC_BASE_URL + token + "/files/types",
      dataType: "json"
    })
    .done(function (filesData) {
      displayTypes(filesData);
    })
    .fail(function () {
      console.log("FILES APP : types list not loaded. An error occured.");
    });
}

/**
 * Log out of app. This will log user out of ChezMeme's SSO destroying the token.
 */
function logout() {
  console.log('FILES APP : function logout');
  $.ajax({
      url: CHEZMEME_BASE_URL + "logout/" + localStorage.getItem("token"),
      dataType: "json"
    })
    .done(function (logoutData) {
      localStorage.removeItem('token');
      window.location.replace("index.html");

      $(document).unbind('mousemove keyup keypressed', function () {
        console.log('FILES APP : Unbinded event used to set timeout');
      });
    })
    .fail(function () {
      console.log("FILES APP : Logout failed");
    });
}

/**
 * Check user activity on app.
 * @param {timeout} : Timeout used to handle inactivity.
 */
function checkUserActivity(timeout) {
  console.log('FILES APP : function checkUserActivity');
  $.ajax({
      url: CHEZMEME_BASE_URL + "who_is/" + localStorage.getItem("token"),
      dataType: "json"
    })
    .done(function (checkUserTokenData) {
      console.log('FILES APP : check token done');
      var isTokenValid = checkUserTokenData.ret === 1 ? true : false;
      if (isTokenValid) {
        console.log('FILES APP : token is valid');
        timeout = setTimeout(function () {
          checkUserActivity(timeout);
        }, 1000 * 60 * 30);
      } else if (!isTokenValid) {
        console.log('FILES APP : token is not valid');
        logout();
      }
    })
    .fail(function (data) {
      console.log('FILES APP : check token failed');
    });
}

/**
 * Display files as a list.
 * @param {filesData} : Data from API.
 */
function displayFiles(data) {
  console.log('FILES APP : function displayFiles()');
  console.log(data);
  for (var i = 0, len = data.length; i < len; i++) {
    var title = data[i].hasOwnProperty('title') ? data[i].title : "";
    var description = data[i].hasOwnProperty('description') ? data[i].description : "";
    var id = data[i].hasOwnProperty('id') ? data[i].id : "";
    var last_file = data[i].hasOwnProperty('last_file') ? data[i].last_file : "";
    var user, first_name, type, last_name, path, version, fileDate, frenchDate;
    if (last_file != null) {
      type = last_file.hasOwnProperty('type') ? last_file.type : '';
      user = last_file.hasOwnProperty('user') ? last_file.user : "";
      first_name = user.hasOwnProperty('first_name') ? user.first_name : "";
      last_name = user.hasOwnProperty('last_name') ? user.last_name : "";
      path = last_file.hasOwnProperty('path') ? last_file.path : "";
      version = last_file.hasOwnProperty('version') ? last_file.version : "";
      // Apply deposit date to the Date function to format date
      fileDate = last_file.hasOwnProperty('deposit_date') ? new Date(last_file.deposit_date.date) : new Date();
      frenchDate = new Intl.DateTimeFormat("fr-FR")
      fileDate = frenchDate.format(fileDate);
    } else {
      first_name = '', last_name = '', path = '', version = '', fileDate = ''
    }

    $("table tbody").append(
      '<tr class="row">' +
      '<td class="col-1"> ' + title + ' </td>' +
      '<td class="col-2"> ' + first_name + ' ' + last_name + '</td>' +
      '<td class="col-2"> ' + description + ' </td>' +
      '<td class="col-2"> ' + type + ' </td>' +
      '<td class="col-1"> ' + version + ' </td>' +
      '<td class="col-2"> ' + fileDate + ' </td>' +
      '<td class="col-2 actions">' +
      '<a href="' + path + '" download=" ' + title + ' " class="btn btn-outline-success" data-toggle="tooltip" data-placement="right" title="Télécharger le fichier"><i class="fa fa-download"></i> Télécharger</a>' +
      '<a href="#" class="updateFile btn btn-outline-primary" data-toggle="tooltip" data-placement="right" title="Importer une version" data-put="' + fileLink + '"><i class="fa fa-upload"></i> Nouveau</a>' +
      '</td>' +
      '</tr>'
    );
    $("#accordion").append(
      '<div class="card">' +
      '<div class="bg-light card-header" id="heading' + i + '">' +
      '<h5 class="mb-0">' +
      '<div class="row p-2" data-toggle="collapse" data-target="#collapse' + i + '" aria-expanded="true" aria-controls="collapse' + i + '">' +
      '<span class="col-10">' + title + '</span>' +
      '<i class="text-primary col-2 fa fa-chevron-circle-down"></i>' +
      '</div>' +
      '</h5>' +
      '</div>' +
      '<div id="collapse' + i + '" class="collapse" aria-labelledby="heading' + i + '" data-parent="#accordion">' +
      '<div class="card-body">' +
      '<div class="row">' +
      '<div class="col-6 font-weight-bold">Propriétaire : </div><div class="col-6">' + first_name + ' ' + last_name + '</div>' +
      '</div>' +
      '<div class="row">' +
      '<div class="col-6 font-weight-bold">Description : </div><div class="col-6">' + description + '</div>' +
      '</div>' +
      '<div class="row">' +
      '<div class="col-6 font-weight-bold">Type de fichier : </div><div class="col-6">' + type + '</div>' +
      '</div>' +
      '<div class="row">' +
      '<div class="col-6 font-weight-bold">Version : </div><div class="col-6">' + version + '</div>' +
      '</div>' +
      '<div class="row">' +
      '<div class="col-6 font-weight-bold">Date de dépôt : </div><div class="col-6">' + fileDate + '</div>' +
      '</div>' +
      '<div class="row">' +
      '<a href="' + path + '" download=" ' + title + ' " class="btn btn-outline-success" data-toggle="tooltip" data-placement="bottom" title="Télécharger le fichier"><i class="fa fa-download"></i> Télécharger</a>' +
      '<a href="#" class="updateFile btn btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="Importer une version"><i class="fa fa-upload"></i> Nouveau</a>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>'
    );

    // Update document uploading a new version
    $('.updateFile').on('click', function () {
      display('upload');
    });
  }
  console.log("FILES APP : Files list loaded");
}

/**
 * Display types in formhtml
 * @param {json} typesData 
 */
function displayTypes(typesData) {
  console.log('FILES APP : function displayTypes');
  for (let i = 0; i < typesData.length; i++) {
    var element = typesData[i];
    console.log(element.type);
    $('#fileType').append('<option value="' + element.id + '">' + element.type + '</option>');
  }
}