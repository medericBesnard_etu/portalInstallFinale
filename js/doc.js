// When DOM is loaded
var token = null;
$( document ).ready(function() {
	
	if($.cookie("token") == null)
		{
		document.location.href="index.html" 
		}
		else
		{
		token = $.cookie("token");
		}
	
  console.log('FILES APP : ready');
  // If the token is stored
  if (token) {
    $.ajax({
      url: CHEZMEME_BASE_URL + "who_is/" + token,
      dataType: "json"
    })
    .done(function( loginData ) {
      if (loginData.ret == 0) {
        logout();
      }
    })
    .fail(function() {
      console.log('FILES APP : filtered research failed');
    });
    console.log('FILES APP : got token - ' + token);
    display('index');
    loadFiles(token);
    loadTypes(token);

    // Set timeout to check user inactivity
    $(document).on('mousemove keyup keypress',function(){
      clearTimeout(inactivityTimeout);
      //do any process and then call the function again
      inactivityTimeout = setTimeout(function () {
        checkUserActivity(inactivityTimeout);
      }, 1000 * 60 * 30); // Check token every 5 minutes
    });
  }
  else {
    logout();
  }

  // Display custom tootlips
  $('[data-toggle="tooltip"]').tooltip();

  // Prevent default behavior on click
  $("#btnFilters").on('click', function(e) {
    e.preventDefault();
  });

  // On logout, go to portal
  $("#btnLogout").on('click', function() {
    window.history.back();
  });

  $("form.search input").on('input change keypress keyup', function () {
    $('#keywords').val($(this).val());
    $('#description').val($(this).val());
  });

  // On submit of filter form, prevent default behavior
  $('#filterForm').submit(function(e) {
    e.preventDefault();
    console.log('FILES APP : filtered research submitted');

    var params = {
      'type' : $('#fileType').val(),
      'version': $('#fileVersion').val(),
      'size': $('#fileSize').val(), 
      'deposit': $('#fileDeposit').val(),
      'keywords': $('#keywords').val().split(' ').join('|'),
      'description': $('#description').val().split(' ').join('|')
    };
    var data = '';

    for (var key in params) {
      if (params.hasOwnProperty(key)) {
        element = params[key];
        if(element !== '' && element !== undefined && element !== 'undefined') data != '' ? data += '&' + key + '=' + element : data += '' + key + '=' + element;
      }
    }
    
    $.ajax({
      url: BASE_DOC_BASE_URL + token + "/files?" + data,
      dataType: "json"
    })
    .done(function( filterFormData ) {
      console.log('FILES APP : filtered research done');
      displayFiles( filterFormData );
    })
    .fail(function() {
      console.log('FILES APP : filtered research failed');
    });
  });

  $('form.search').submit(function (e) {
    e.preventDefault();
    $.ajax({
      url: BASE_DOC_BASE_URL + token + "/files?" + $('#searchInput').val(),
      dataType: "json"
    })
    .done(function( filterFormData ) {
      console.log('FILES APP : filtered research done');
      displayFiles( filterFormData );
    })
    .fail(function() {
      console.log('FILES APP : filtered research failed');
    });
  });

  // Add new version of a file
  $('.updateFile').on('click', function(e) {
    e.preventDefault();
    console.log('data-put : ' + $(this).attr('data-put'));
    display('upload');
    // Fill the form with file informations
    $('#form').submit(function(e) {
      e.preventDefault();
      console.log('test');
    });
  });

});
